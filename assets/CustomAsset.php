<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CustomAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
        'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
        /*'/js/ie-emulation-modes-warning.js',*/
        '/js/ie10-viewport-bug-workaround.js',
        '/js/modernizr.min.js',
        '/js/main.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
