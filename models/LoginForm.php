<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;
    public $verifyCode;

    private $_user = false;

    const SCENARIO_WITH_CAPTCHA = 'with-captcha';
    const LOGIN_ATTEMPT_SESSION_KEY = 'login-attempt';
    const MAX_ATTEMPTS_WO_CAPTCHA = 2;

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT      => ['email', 'password', 'rememberMe'],
            self::SCENARIO_WITH_CAPTCHA => ['email', 'password', 'rememberMe', 'verifyCode'],
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'captchaAction' => '/site/captcha', 'on' => self::SCENARIO_WITH_CAPTCHA],
            ['verifyCode', 'required', 'on' => self::SCENARIO_WITH_CAPTCHA],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный email или пароль.');
            }
        }
    }


    public function validateEmail($attribute, $params)
    {

        if ($this->hasErrors())
            return;
        if ($User = User::find()->where(['email' => $this->email])->one()) {
            $this->_user = $User;

            return;
        }

        $this->addError($attribute, 'Пользователь с таким email не найден.');
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate())
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->_user ?: User::findByEmail($this->email);
    }

    public function attributeLabels()
    {
        return [
            'email'      => 'Email',
            'password'   => 'Пароль',
            'rememberMe' => 'Запомнить меня?',
            'verifyCode' => 'Проверочный код',
        ];
    }
}
