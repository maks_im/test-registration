<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ApplicationForm extends Model
{
    public $email;
    public $email_confirm;
    public $password;
    public $password_confirm;
    public $name;
    public $surname;
    public $phone;
    public $referral_code_entered;
    public $verifyCode;

    // passwordStrength
    const PASSWORD_WEAK = 1;
    const PASSWORD_STRONG = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'phone', 'email', 'referral_code_entered'], 'string', 'max' => 255],
            ['email', 'email'],
            [['name', 'surname', 'phone', 'email', 'password', 'password_confirm', 'verifyCode'], 'required'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'captchaAction' => '/site/captcha'],
            ['password', 'passwordStrength', 'params' => ['strength' => self::PASSWORD_STRONG]/*, 'except' => ['update', 'import']*/],
            ['password_confirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Введенные пароли не совпадают.'],

            [
                'email', function ($attribute, $params) {
                if (!$this->hasErrors())
                    if (User::find()->where([$attribute => $this->$attribute])->exists())
                        $this->addError($attribute, 'Пользователь с таким email уже зарегистрирован. Введите другой email.');
            },
            ],
        ];
    }

    public function passwordStrength($attribute, $params)
    {
        if ($params['strength'] === self::PASSWORD_STRONG) {
            $pattern = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/';
            if (!preg_match($pattern, $this->$attribute))
                $this->addError($attribute, "Слишком простой пароль! Пароль должен быть не короче 8 символов и содержать одну заглавную букву, одну строчную и одну цифру.");
        } else {
            $pattern = '/^(?=.*[a-zA-Z0-9]).{5,}$/';
            if (!preg_match($pattern, $this->$attribute))
                $this->addError($attribute, "Слишком простой пароль! Пароль должен быть не короче 5 символов.");
        }
    }
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email'            => 'Email',
            'password'         => 'Пароль',
            'password_confirm' => 'Пароль еще раз',
            'name'             => 'Имя',
            'surname'          => 'Фамилия',
            'phone'            => 'Мобильный телефон',
            'verifyCode'       => 'Проверочный код',
        ];
    }

    /**
     * Метод регистрации нового пользователя
     *
     * Метод валидирует модель ApplicationForm ($this) и возвращает модель пользователя (User),
     * если запись в базу прошла успешно
     *
     * @return User|null
     */
    public function apply()
    {
        if ($this->validate()) {
            $User = new User();
            $User->setAttributes($this->getAttributes());
            $User->setPassword($this->password);

            if ($User->save())
                return $User;
        }

        return null;
    }
}
