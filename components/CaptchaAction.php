<?php

namespace app\components;

class CaptchaAction extends \yii\captcha\CaptchaAction
{
    public $testLimit = 1;
    public $backColor = 0xFFFFFF;
    public $foreColor = 0x666666;
    public $width = 120;
}
