<?php

/** @var User $User */
use app\models\User;

$this->title = 'Главная';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="well">
            <h1>Вы успешно авторизировались.</h1>
            <p>Ваш email: <?= Yii::$app->user->identity->email ?></p>
        </div>
    </div>
</div>