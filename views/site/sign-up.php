<?php
/**
 * @var ApplicationForm $ApplicationForm
 */
use app\models\ApplicationForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="page-header text-center"><?= $this->title ?><br>
    <small>Пожалуйста, введите свои контактные данные</small>
</h1>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="well">
            <?php
            $form = ActiveForm::begin([
                'id'          => 'application-form',
                'options'     => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template'     => "{label}\n<div class=\"col-md-6\">{input}</div>\n<div class=\"col-md-offset-6 col-md-6\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-6 control-label'],
                ],
            ]) ?>

            <?= $form->field($ApplicationForm, 'email') ?>
            <?= $form->field($ApplicationForm, 'password')->passwordInput() ?>
            <?= $form->field($ApplicationForm, 'password_confirm')->passwordInput(['class' => 'no-cut-copy-paste form-control']) ?>
            <?= $form->field($ApplicationForm, 'name') ?>
            <?= $form->field($ApplicationForm, 'surname') ?>
            <?= $form->field($ApplicationForm, 'phone') ?>

            <?= $form->field($ApplicationForm, 'verifyCode')->widget(Captcha::className(), [
                'captchaAction' => '/site/captcha',
                'template'      => '<div class="col-md-12">{image}</div><div class="col-md-12">{input}</div>',
            ]) ?>

            <div class="text-center">
                <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
