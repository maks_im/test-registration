<?php


/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="well">
            <div class="text-center">
                <h3>Вход</h3>
            </div>
            <?php $form = ActiveForm::begin([
                'id'          => 'login-form',
                'options'     => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template'     => "{label}\n<div class=\"col-md-12\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
                    'labelOptions' => ['class' => 'col-md-1 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?php if (Yii::$app->session->get($model::LOGIN_ATTEMPT_SESSION_KEY) >= $model::MAX_ATTEMPTS_WO_CAPTCHA) { ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'captchaAction' => '/site/captcha',
                    'template'      => '<div class="col-md-6">{image}</div><div class="col-md-6">{input}</div></div>',
                ]) ?>
            <?php } ?>

            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-md-12\">{input} {label}</div>\n<div class=\"col-md-12\">{error}</div>",
            ]) ?>

            <div class="form-group">
                <div class="col-md-12 text-center">
                    <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>