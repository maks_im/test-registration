<?php

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => 'Тест',
    'brandUrl'   => Yii::$app->homeUrl,
    'options'    => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items'   =>
        Yii::$app->user->isGuest ? ([
            ['label' => 'Вход', 'url' => ['/site/login'], 'options' => ['class' => Yii::$app->request->url == "/site/login" ? 'active' : '']],
            ['label' => 'Регистрация', 'url' => ['/site/sign-up'], 'options' => ['class' => Yii::$app->request->url == "/site/admissions" ? 'active' : '']],
        ]) : [
            Html::a("Выйти", ['/site/logout'], [
                'data'  => [
                    'method'  => 'post',
                    'confirm' => "Выйти?",
                    'pjax'    => '0',
                ],
                'class' => 'btn btn-md btn-danger',
                'style' => 'margin-top: 8px;',
            ])]
]);
NavBar::end();