<?php

namespace app\controllers;

use app\models\ApplicationForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'app\components\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Главная
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest)
            return $this->redirect('/site/login');

        return $this->render('index', ['User' => Yii::$app->user->identity]);
    }

    /**
     * Запись
     *
     * @return string
     */
    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();
        /** @var ApplicationForm $ApplicationForm */
        $ApplicationForm = new ApplicationForm();
        if (Yii::$app->request->isPost) {

            $ApplicationForm->load(Yii::$app->request->post());

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($ApplicationForm);
            }

            /** @var User $User */
            if ($User = $ApplicationForm->apply()) {
                Yii::$app->user->login($User, 3600 * 24 * 30);

                return $this->redirect('/');
            }

            /*
            Yii::$app->mailer->compose('welcome')
                            ->setFrom('robot@корпоративный_домен.ru')
                            ->setTo($User->email)
                            ->setSubject('Регистрация')
                            ->send();
            */

        }

        return $this->render('sign-up', [
            'ApplicationForm' => $ApplicationForm,
        ]);
    }


    /**
     * Логин
     *
     * @return string
     */
    public
    function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $LoginForm = new LoginForm();
        $session = Yii::$app->session;
        $attempts = (int) $session->get(LoginForm::LOGIN_ATTEMPT_SESSION_KEY, 0);
        if ($attempts >= LoginForm::MAX_ATTEMPTS_WO_CAPTCHA)
            $LoginForm->scenario = LoginForm::SCENARIO_WITH_CAPTCHA;
        if ($LoginForm->load(Yii::$app->request->post())) {
            if ($LoginForm->login()) {
                Yii::$app->session->remove(LoginForm::LOGIN_ATTEMPT_SESSION_KEY);

                return $this->goBack();
            } else
                Yii::$app->session->set(LoginForm::LOGIN_ATTEMPT_SESSION_KEY, $attempts + 1);
        }

        return $this->render('login', [
            'model' => $LoginForm,
        ]);
    }

    /**
     * Выход
     *
     * @return string
     */
    public function actionLogout()
    {
        return Yii::$app->user->logout() && $this->goHome();
    }
}
